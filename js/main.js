const myTabs = document.getElementsByClassName('tabs-title');
for (let i = 0; i < myTabs.length; i++) {
    myTabs[i].addEventListener('click', switchTabs);
}

function switchTabs() {
    // const tabs = this;
    // const tabsChildren = tabs.children;
    const tabs = document.getElementsByClassName('tabs');
    const tabsChildren = tabs.children;
    for (let i = 0; i < tabsChildren.length; i++) {
        tabsChildren[i].classList.remove('active');
    }
    this.classList.add('active');

    const showTabs = document.getElementById(this.dataset.target);
    const show = showTabs.parentElement;
    const showChildren = show.children;
    for (let i = 0; i < showChildren.length; i++) {
        showChildren[i].classList.remove('active');
    }
    showTabs.classList.add('active');
}
